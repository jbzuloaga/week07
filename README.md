# week07

## Getting started

First part of the assigment in "week07-1" branch.
Second part of the assigment in "week07-2" branch.

## Week07-1

In order to run the project, before execute ng serve, you need to run npm install. This project use a country-state-city: https://www.npmjs.com/package/country-state-city

## Week07-2

In order to run the project, just execute ng serve.
